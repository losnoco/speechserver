import Vapor
import HTTP
import AppKit
import AVFoundation

let drop = Droplet()

class SpeechSynth: NSObject, NSSpeechSynthesizerDelegate {
    var synth : NSSpeechSynthesizer!
    
    init(withVoice voice: String) {
        self.synth = NSSpeechSynthesizer.init(voice: voice)
    }
    
    func speak(theString string: String) {
        self.synth.delegate = self
        self.synth.startSpeaking(string)
        while(NSSpeechSynthesizer.isAnyApplicationSpeaking() == true) {}
    }
    
    func speakToData(theString string: String) throws -> Data {
        self.synth.delegate = self
        let directory = NSTemporaryDirectory()
        let fileName = NSUUID().uuidString
        
        // This returns a URL? even though it is an NSURL class method
        let fullURL = NSURL.fileURL(withPathComponents: [directory, fileName])
        self.synth.startSpeaking(string, to: fullURL!)
        while(NSSpeechSynthesizer.isAnyApplicationSpeaking() == true) {}
        return try Data(contentsOf: fullURL!)
    }
}



func getVoices() -> [[String:String]] {
    let voices = NSSpeechSynthesizer.availableVoices()
    var voiceDict: [[String:String]] = []
    
    for voice in voices {
        let voiceAttribs = NSSpeechSynthesizer.attributes(forVoice: voice)
        var currentItem = [String:String]()
        let voiceLocale = NSLocale(localeIdentifier: String(describing: voiceAttribs[NSVoiceLocaleIdentifier]!))
        let currentLocale = NSLocale(localeIdentifier: "en_US")
        let gender = String(describing: voiceAttribs[NSVoiceGender]!);
        
        
        if #available(OSX 10.12, *) {
            currentItem = [
                "identifier":   String(describing: voiceAttribs[NSVoiceIdentifier]!),
                "name":         String(describing: voiceAttribs[NSVoiceName]!),
                "age":          String(describing: voiceAttribs[NSVoiceAge]!),
                "language":     String(describing: voiceAttribs[NSVoiceLocaleIdentifier]!),
                "demoText":     String(describing: voiceAttribs[NSVoiceDemoText]!),
                "gender":       gender.replacingOccurrences(of: "VoiceGender", with: "").lowercased(),
                "nativeName":   voiceLocale.localizedString(forLocaleIdentifier: voiceLocale.localeIdentifier),
                "englishName":   currentLocale.localizedString(forLocaleIdentifier: voiceLocale.localeIdentifier)
                
            ]
        } else {
            // Fallback on earlier versions
        }
        voiceDict.append(currentItem)
        
    }
    return voiceDict
}

drop.get("voices") {req in
    let voiceDict = getVoices();
    
    let node = try voiceDict.flatMap { try $0.makeNode() }.makeNode()

    return JSON(node)
}

drop.get { req in

    return try drop.view.make("main.html")
}

drop.post("speak") { req in
    let voice = req.data["voice"]?.string
    let text = req.data["text"]?.string
    let rate = req.data["rate"]?.int
    
    if (voice == nil) {
        return Response(status: .badRequest)
    }
    
    if (rate == nil) {
        return Response(status: .badRequest)
    }
    
    if (text == nil) {
        return Response(status: .badRequest)
    }

    var speechSynth = SpeechSynth(withVoice: voice!);
    
    speechSynth.synth.rate = Float(rate!);
    
   
    var out = try speechSynth.speakToData(theString: text!)
    
    var x: Response = HTTP.Response(
        status: .ok,
        headers: [
            "Content-Type": "audio/aiff"
        ],
        body: .data([UInt8](out))
    );
    
    return x;
    
    
    
}


drop.run()
